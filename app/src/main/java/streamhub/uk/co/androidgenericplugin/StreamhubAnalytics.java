package streamhub.uk.co.androidgenericplugin;

import android.content.Context;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;


/**
 * Created by tony on 11/01/16.
 */
public class StreamhubAnalytics {

    float _startTime                            = 0.0f;
    String _channelId;
    String _publicId;
    String _partnerId;
    String _endPoint                            = "https://stats.streamhub.io/api/";
    String _playerId;
    Boolean _isLive;
    String _userId;
    String _analyticsId;
    String _sessionId;
    String _randomSessionKey;
    String[] _completionRatesLabels             = new String[] { "1", "25", "50", "75", "95" };
    float[] _trackableCompletionRates           = new float[] { 0.01f, 0.25f, 0.50f, 0.75f, 0.95f };
    int[] _completionRateStopPoints             = new int[] { 0, 0, 0, 0, 0 };
    float _mediaDuration                        = -1.f;

    HashMap<String, String> mediaMetadata       = new HashMap<>();
    HashMap<Float, Boolean> _tickerMap          = new HashMap<Float, Boolean>();
    HashMap<Integer, Boolean> _completionMap    = new HashMap<Integer, Boolean>();

    NetworkUI _networkUI;

    public static String DURATION               = "duration";
    public static String CATEGORIES             = "categories";
    public static String PLAYER_TITLE           = "playerTitle";
    public static String TITLE                  = "title";

    private static final String TAG             = StreamhubAnalytics.class.getSimpleName();

    public StreamhubAnalytics(Context context, String partnerId, String endPoint, String playerId, Boolean isLive,
                              String userId, String analyticsId) {


        // Get SessionId and generate RandomSessionKey
        _sessionId          = Settings.Secure.getString( context.getContentResolver(), Settings.Secure.ANDROID_ID);
        _randomSessionKey   = UUID.randomUUID().toString();

        _partnerId          = partnerId;

        if(!endPoint.isEmpty() && endPoint != null) {
            _endPoint       = endPoint;
        }

        _playerId           = playerId;
        _isLive             = isLive;
        _userId             = userId;
        _analyticsId        = analyticsId;
        _channelId          = "";

        _networkUI          = new NetworkUI( context, _endPoint );

        Log.d(TAG, "SessionId: " + _sessionId );
    }

    public void reset(){
        _mediaDuration      = -1.f;
        _tickerMap          = new HashMap<Float, Boolean>();
        _completionMap      = new HashMap<Integer, Boolean>();
        mediaMetadata       = new HashMap<>();
    }

    public void setChannelId(String channelId){
        _channelId = channelId;
    }

    public void setDuration(Float duration){

        if( _mediaDuration < 0 && duration > 0 ){

            _mediaDuration = duration;

            // Provide duration to addMediaMetadata
            addMediaMetadata( StreamhubAnalytics.DURATION, String.valueOf(duration.intValue()) );
        }

        Log.d(TAG, "duration: " + duration);

        for (int i = 0 ; i < _trackableCompletionRates.length ; i++)
        {

            float rate                      = _trackableCompletionRates[ i ];
            int completionRateStopPoint     = (int)(_mediaDuration * rate);

            if(completionRateStopPoint == 0) {

                _completionRateStopPoints[ i ]     = 1;
            }
            else {
                _completionRateStopPoints[ i ]     = completionRateStopPoint;
            }
        }

        Log.d(TAG, "is using completion rates reporting. \n" +
                "Completion rate events should be sent at rates: " + Arrays.toString(_completionRateStopPoints) );
    }

    /**
     * onTick is a convenient method that abstract the logic of measuring accurately the video
     watching time.
     the AVFoundation framework provides different way to read the video playback current time
     (see sample apps).
     This is the perfect place to call the onTick api method.
     You should call onTick during the whole duration of the video playback.
     The unique parameter has to be provided in seconds.
     * @param playbackPositionInSec
     */
    public void onTick(float playbackPositionInSec){

        int roundedPlaybackPositionInSec    = (int)( playbackPositionInSec );
        float startTime                     = (float)roundedPlaybackPositionInSec / 60.0f;

        Log.d( TAG, "onTick, playbackPositionInSec: " + playbackPositionInSec +
                ", roundedPlaybackPositionInSec: " + roundedPlaybackPositionInSec +
                ", startTime : " + startTime );

        // send ticks every 5 secs for [0mn - 1mn], every minutes after
        this.trackDefaultStrategy(roundedPlaybackPositionInSec, startTime);

        if( _mediaDuration > 0 ){ // Evaluate the need for completion rate reporting

            for (int i = 0 ; i < _completionRateStopPoints.length ; i++) {

                int stopPoint = _completionRateStopPoints[ i ];

                if( roundedPlaybackPositionInSec == stopPoint ){

                    if (_completionMap.containsKey( stopPoint ) )
                    {
                        return;
                    } else
                    {
                        String matchingRateLabel        = _completionRatesLabels[ i ];
                        _completionMap.put( stopPoint, true );

                        // Send event
                        HashMap<String, String> completionEventParams  = new HashMap<String, String>();
                        completionEventParams.put( "completionRate", matchingRateLabel );


                        _networkUI.sendEvent(NetworkUI.MEDIA_COMPLETION, completionEventParams, _startTime, _publicId,
                                _partnerId, _analyticsId, _playerId, _isLive, _randomSessionKey, _sessionId, _userId);

                        Log.d( TAG, "Sending completion rate for matchingRateLabel: " +
                                matchingRateLabel );
                    }
                }

            }
        }
    }

    public void trackDefaultStrategy( int rpps, float startTime){

        Log.d( TAG, "trackDefaultStrategy: " + rpps + ", startTime: " + startTime  );

        if( (rpps >= 60 && rpps % 60 == 0) || // >=1mn => tick every mn
            (rpps < 60 && rpps % 5 == 0)) // <1mn => tick every 5sec
        {
            trackTicker(startTime);
        }
    }

    public void trackTicker(float startTime) {

        Log.d( TAG, "trackTicker: " + startTime + ", contained key: " + _tickerMap.containsKey(startTime) );

        if (_tickerMap.containsKey(startTime)) {
            return;
        }

        Log.d( TAG, "trackTicker: " + startTime );

        _tickerMap.put( startTime, true);
        _startTime              = startTime;

        // Send tick
        _networkUI.sendTick(startTime, _publicId, _partnerId, _analyticsId, _playerId, _isLive, _randomSessionKey,
                _sessionId, _userId, _channelId);
    }

    /**
     * API playerevent
     * must be called to provide the unique identifier of the media about to be
     played.
     * @param publicId
     */
    public void onMediaReady(String publicId) {

        if(publicId.isEmpty()) {

            Log.d( TAG, "Missing mandatory parameter publicId." );
            return;
        }

        _publicId   = publicId;
    }

    /**
     * That should be the first event your tracker sends.
     * Signals the beginning of a playback session
     * A new randomSessionKey identifier is created
     */
    public void onMediaLoaded() {

        _randomSessionKey   = UUID.randomUUID().toString();

        Log.d(TAG, "Generated new _randomSessionKey: " + _randomSessionKey );

        _networkUI.sendEvent(NetworkUI.MEDIA_LOADED, null, _startTime, _publicId,
                _partnerId, _analyticsId, _playerId, _isLive, _randomSessionKey, _sessionId, _userId);
    }

    /**
     * must be called the first time your video content starts to play.
     */
    public void onMediaStart() {

        _networkUI.sendEvent(NetworkUI.MEDIA_START, null, _startTime, _publicId,
                _partnerId, _analyticsId, _playerId, _isLive, _randomSessionKey, _sessionId, _userId);
    }

    /**
     * Call onMediaComplete to report a video completed event.
     The state of the plugin is reset when you call onMediaComplete.
     As a result, onMediaReady, onMediaStart, setDuration should be called again to process the
     next video if any.
     */
    public void onMediaComplete() {

        _networkUI.sendEvent(NetworkUI.MEDIA_COMPLETE, null, _startTime, _publicId,
                _partnerId, _analyticsId, _playerId, _isLive, _randomSessionKey, _sessionId, _userId);

        reset();
    }

    /** Deprecated. Made useless as we can store/retrieve a unique ID with
     * Settings.Secure.getString( context.getContentResolver(), Settings.Secure.ANDROID_ID); */
    private String getSessionId(Context con){

        try {
            TelephonyManager tm = (TelephonyManager)con.getSystemService(Context.TELEPHONY_SERVICE);
            String tmDevice, tmSerial, androidId;

            tmDevice    = "" + tm.getDeviceId();
            tmSerial    = "" + tm.getSimSerialNumber();
            androidId   = "" + android.provider.Settings.Secure.getString(con.getContentResolver(),
                    android.provider.Settings.Secure.ANDROID_ID);

            UUID deviceUuid = new UUID(androidId.hashCode(),
                    ((long)tmDevice.hashCode() << 32) | tmSerial.hashCode());
            return deviceUuid.toString();
        }
        catch(SecurityException se){
            Log.d(TAG, "SecurityException | " + se.getMessage() + " in StreamhubAnalytics::getSessionId");
        }
        finally {
            return null;
        }
    }

    public void trackAd(HashMap<String, String> adInfo, int percentile)
    {
        if(adInfo != null && !_publicId.isEmpty()){
            _networkUI.sendAdEvent(NetworkUI.MEDIA_COMPLETION, adInfo, percentile, _publicId,
                    _partnerId, _analyticsId, _playerId, _randomSessionKey, _sessionId);
        }
    }

    public void onAdClick(HashMap<String, String> adInfo, int percentile)
    {
        if(adInfo != null && !_publicId.isEmpty()) {
            _networkUI.sendAdEvent(NetworkUI.AD_CLICK_THROUGH, adInfo, percentile, _publicId,
                    _partnerId, _analyticsId, _playerId, _randomSessionKey, _sessionId);
        }
    }

    public void onMediaBufferedComplete( float bufferingTime, boolean prebuffering ){

        if( bufferingTime > 0 ){

            String eventName;
            if(prebuffering){
                eventName = NetworkUI.MEDIA_PRE_BUFFERING;
            } else {
                eventName = NetworkUI.MEDIA_BUFFERING;
            }

            HashMap<String, String> bufferingEventParams  = new HashMap<String, String>();
            bufferingEventParams.put( "bufferingTime", new Float(bufferingTime).toString() );

            _networkUI.sendEvent(eventName, bufferingEventParams, _startTime, _publicId,
                    _partnerId, _analyticsId, _playerId, _isLive, _randomSessionKey, _sessionId, _userId);
        }
    }

    public boolean addMediaMetadata(String key, String value) {


        if(key == StreamhubAnalytics.CATEGORIES){

            String[] categories         = value.split("|");
            ArrayList<String> catList   = new ArrayList<>();

            try {
                for(String cat : categories){

                    // Since The space character "   " is converted into a plus sign "+"
                    // https://developer.android.com/reference/java/net/URLEncoder.html
                    catList.add( URLEncoder.encode(cat, "utf-8") );
                }
            }
            catch(UnsupportedEncodingException uee) {}


            String urlEncodedAndJoinedCategories =  TextUtils.join("|", catList);
            mediaMetadata.put( StreamhubAnalytics.CATEGORIES, urlEncodedAndJoinedCategories );

            Log.d(TAG, "addMediaMetadata | " + urlEncodedAndJoinedCategories);

            return true;
        }
        else if (key == StreamhubAnalytics.PLAYER_TITLE || key == StreamhubAnalytics.TITLE || key == StreamhubAnalytics.DURATION) {

            try {
                // Since The space character "   " is converted into a plus sign "+"
                // https://developer.android.com/reference/java/net/URLEncoder.html
                mediaMetadata.put( key,
                        URLEncoder.encode(value, "utf-8") );
            }
            catch(UnsupportedEncodingException uee) {}

            Log.d(TAG, "addMediaMetadata | " + key + " -> " + URLEncoder.encode(value));

            return true;
        }

        return false;
    }

    public void onMediaMetadata(){

        if( mediaMetadata.containsKey(StreamhubAnalytics.TITLE) &&
            mediaMetadata.containsKey(StreamhubAnalytics.PLAYER_TITLE) ) {

                _networkUI.sendEvent(NetworkUI.MEDIA_METADATA, mediaMetadata, _startTime, _publicId,
                        _partnerId, _analyticsId, _playerId, _isLive, _randomSessionKey, _sessionId, _userId);

        } else {
            throw new Error("Your mediaMetadata object is missing some of the required properties: title, playerTitle, duration." +
                    "Please ensure you are calling addMediaMetadata() prior to calling onMediaMetadata() ");
        }

    }










}
