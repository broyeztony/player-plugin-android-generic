package streamhub.uk.co.androidgenericplugin;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class NetworkUI {

    private final ApiInterface api;
    String _AAID                        = "";

    // Events Constants
    static String MEDIA_ERROR           = "player_error";
    static String MEDIA_LOADED          = "media_loaded";
    static String MEDIA_METADATA        = "media_metadata";
    static String MEDIA_START           = "player_start";
    static String MEDIA_COMPLETION      = "completion";
    static String MEDIA_PRE_BUFFERING   = "prebuffering";
    static String MEDIA_BUFFERING       = "buffering";
    static String AD_CLICK_THROUGH      = "click";
    static String MEDIA_COMPLETE        = "player_play_completed";

    private static final String TAG    = NetworkUI.class.getSimpleName();


    public NetworkUI(final Context context, String apiEndpoint){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(apiEndpoint)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
        api = retrofit.create(ApiInterface.class);

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    AdvertisingIdClient.Info adInfo = AdvertisingIdClient.getAdvertisingIdInfo( context );
                    String adId = adInfo != null ? adInfo.getId() : null;

                    Log.d( TAG, "AdvertisingIdClient.Info isLimitAdTrackingEnabled-> " + adInfo.isLimitAdTrackingEnabled() );
                    Log.d( TAG, "AdvertisingIdClient.Info getId -> " + adInfo.getId() );

                    _AAID = adInfo.getId();

                } catch (IOException |
                        GooglePlayServicesRepairableException |
                        GooglePlayServicesNotAvailableException exception) {
                    // Error handling if needed
                }
            }
        });

        Log.d( TAG, "[NetworkUI initialized with: " + apiEndpoint );
    }

    public String getAIID() {
        return _AAID;
    }


    public String osDevice(){
        String os   = Build.MANUFACTURER + " " + Build.MODEL + ", Android " + Build.VERSION.RELEASE;
        return os;
    }

    public static String userAgent(){

        Log.d( TAG, "Build.ID:" + Build.ID );

        return "Android; Brand: " + Build.BRAND + "; Product: " + Build.PRODUCT + "; OS: " + Build.VERSION.RELEASE;
    }

    public void sendTick(float startTime, String publicId, String partnerId, String analyticsId,
                         String playerId, boolean isLive, String randomSessionKey,
                         String sessionId, String userId, String channelId ){

        String refUrl                           = "";
        String locationUrl                      = "";
        String bitrate                          = "";
        String agent                            = NetworkUI.userAgent();

        Call<String> call = api.playerApi(
                String.valueOf(startTime),
                publicId,
                partnerId,
                analyticsId,
                playerId,
                String.valueOf(isLive),
                sessionId,
                randomSessionKey,
                osDevice(),
                refUrl,
                locationUrl,
                agent,
                bitrate,
                userId,
                channelId,
                _AAID
        );
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                Log.d(TAG, "player: Success: " + response.toString());
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d(TAG, "player: Error: " + t.getMessage());
            }
        });

        Log.d(TAG, call.request().url().toString());
    }

    public void sendEvent(String eventName, HashMap<String, String> eventParameters,
                          float startTime, String publicId, String partnerId, String analyticsId,
                          String playerId, boolean isLive, String randomSessionKey,
                          String sessionId, String userId ) {

        Call<String> call = api.playerEventApi(
                eventName,
                String.valueOf(startTime),
                analyticsId,
                partnerId,
                publicId,
                playerId,
                sessionId,
                randomSessionKey,
                osDevice(),
                NetworkUI.userAgent(),
                _AAID,
                eventParameters == null ? new HashMap<>() : eventParameters
        );
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, retrofit2.Response<String> response) {

                Log.d(TAG, "playerEvent: Success : " + response.toString());
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d(TAG, "playerEvent: Error: " + t.getMessage());
            }
        });

        Log.d(TAG, call.request().url().toString());
    }

    public void sendAdEvent(String eventName, HashMap<String, String> adInfo,
            int percentile, String parentPublicId, String partnerId, String analyticsId,
            String playerId, String randomSessionKey,
            String sessionId) {

        String publicId = adInfo.get("id");

        HashMap<String, String> options = new HashMap<>();
        Iterator it = adInfo.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();

            if (pair.getKey() == "campaignId" ||
                    pair.getKey() == "distributorId" ||
                    pair.getKey() == "advertiserId" ||
                    pair.getKey() == "code" ||
                    pair.getKey() == "customField") {

                options.put((String) pair.getKey(), (String) pair.getValue());
                it.remove(); // avoids a ConcurrentModificationException
            }
        }

        Call<String> call = api.advertisementApi(
                eventName,
                String.valueOf(percentile),
                analyticsId,
                partnerId,
                publicId,
                playerId,
                sessionId,
                randomSessionKey,
                NetworkUI.userAgent(),
                parentPublicId,
                _AAID,
                options
        );
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                Log.d(TAG, "advertisement: Success: " + response.toString());
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d(TAG, "advertisement: Error: " + t.getMessage());
            }
        });

        Log.d(TAG, call.request().url().toString());
    }


}
