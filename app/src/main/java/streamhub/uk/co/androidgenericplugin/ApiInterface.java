package streamhub.uk.co.androidgenericplugin;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface ApiInterface {

    @GET("player")
    Call<String> playerApi(
            @Query("startTime") String startTime,
            @Query("publicId") String publicId,
            @Query("partnerId") String partnerId,
            @Query("analyticsId") String analyticsId,
            @Query("playerId") String playerId,
            @Query("isLive") String isLive,
            @Query("sessionId") String sessionId,
            @Query("randomSessionKey") String randomSessionKey,
            @Query("os") String os,
            @Query("refUrl") String refUrl,
            @Query("locationUrl") String locationUrl,
            @Query("agent") String agent,
            @Query("bitrate") String bitrate,
            @Query("userId") String userId,
            @Query("channelId") String channelId,
            @Query("advertisingId") String advertisingId
    );

    @GET("playerevent")
    Call<String> playerEventApi(
            @Query("event") String event,
            @Query("startTime") String startTime,
            @Query("analyticsId") String analyticsId,
            @Query("partnerId") String partnerId,
            @Query("publicId") String publicId,
            @Query("playerId") String playerId,
            @Query("sessionId") String sessionId,
            @Query("randomSessionKey") String randomSessionKey,
            @Query("os") String os,
            @Query("agent") String agent,
            @Query("advertisingId") String advertisingId,
            @QueryMap HashMap<String, String> options
    );

    @GET("advertisement")
    Call<String> advertisementApi(
            @Query("event") String event,
            @Query("percentile") String percentile,
            @Query("analyticsId") String analyticsId,
            @Query("partnerId") String partnerId,
            @Query("publicId") String publicId,
            @Query("playerId") String playerId,
            @Query("sessionId") String sessionId,
            @Query("randomSessionKey") String randomSessionKey,
            @Query("agent") String agent,
            @Query("parentPublicId") String parentPublicId,
            @Query("advertisingId") String advertisingId,
            @QueryMap HashMap<String, String> options
    );

}
