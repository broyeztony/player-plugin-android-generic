package streamhub.uk.co.androidgenericplugin;

import android.media.MediaPlayer;
import android.util.Log;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity implements MediaPlayer.OnPreparedListener,
        MediaPlayer.OnCompletionListener, Runnable {

    private static final String TAG = MainActivity.class.getSimpleName();
    private MediaPlayer mediaPlayer;
    private ScheduledThreadPoolExecutor playheadUpdateScheduler;
    private boolean _hasCalledOnMediaStart = false;
    private StreamhubAnalytics streamhub;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Log.d(TAG, "[StreamhubAnalytics Generic Android Plugin Test Application]");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String url    = "http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4";


        // #1 Create a StreamhubAnalyticsObject
        streamhub   = new StreamhubAnalytics(this, "your-platform", "",
                "android-generic-test-player", false,
                "logged-user-id", "streamhub-5812d");

        // #2 plug into player events
        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setDataSource(url);
            mediaPlayer.setOnPreparedListener(this);
            mediaPlayer.setOnCompletionListener(this);
            mediaPlayer.prepareAsync();

            mediaPlayer.setOnInfoListener(new MediaPlayer.OnInfoListener() {

                float _start;

                @Override
                public boolean onInfo(MediaPlayer mp, int what, int extra) {
                    switch (what) {
                        case MediaPlayer.MEDIA_INFO_BUFFERING_START:

                                Log.d(TAG, "MEDIA_INFO_BUFFERING_START" );

                                _start = System.currentTimeMillis() / 1000L;

                            break;
                        case MediaPlayer.MEDIA_INFO_BUFFERING_END:

                                Log.d(TAG, "MEDIA_INFO_BUFFERING_END" );

                                // Report a media buffering event
                                float bufferingTime = (System.currentTimeMillis() / 1000L) - _start;

                                // Call onMediaBufferedComplete to signal the end of a buffering sequence
                                // 2nd parameter signals if this is a prebuffering (before the frame's rendered) or a rebuffering during playback
                                streamhub.onMediaBufferedComplete( bufferingTime, _hasCalledOnMediaStart == false );

                            break;
                    }
                    return false;
                }
            });


        }
        catch(IOException ioe){
            Log.d( TAG, "IOException " + ioe.getMessage() );
        }
    }

    /** Called when MediaPlayer is ready */
    public void onPrepared(MediaPlayer player) {


        streamhub.onMediaReady("sara-screen-recording");
        streamhub.onMediaLoaded();

        mediaPlayer.start();

        // #3 start tracking and reporting event
        float duration = player.getDuration() / 1000;

        // Track an ad
        HashMap<String, String> adInfo = new HashMap<>();
        adInfo.put("id", "6128");
        adInfo.put("campaignId", "484");
        adInfo.put("advertiserId", "91");
        streamhub.trackAd(adInfo, 0);
        streamhub.trackAd(adInfo, 25);
        streamhub.trackAd(adInfo, 50);
        streamhub.trackAd(adInfo, 75);
        streamhub.trackAd(adInfo, 100);

        // Save the media's public identifier for completion rate tracking
        streamhub.setDuration(duration);

        // Report the first event -> media has started playback
        streamhub.onMediaStart();
        _hasCalledOnMediaStart = true;


        // Provide media metadata to Streamhub. You can add up to 10 metadata fields
        streamhub.addMediaMetadata( StreamhubAnalytics.TITLE, "Big Buck Bunny" );
        streamhub.addMediaMetadata( StreamhubAnalytics.PLAYER_TITLE, "my awesome player" );
        streamhub.addMediaMetadata( StreamhubAnalytics.CATEGORIES, "some|meaningful|categories" );
        streamhub.onMediaMetadata();

        // 2) Start monitoring playhead's update to track minutely ticks
        playheadUpdateScheduler = new ScheduledThreadPoolExecutor(1);
        playheadUpdateScheduler.scheduleAtFixedRate(this, 0, 500, TimeUnit.MILLISECONDS); // execute every 60 seconds
    }

    /** Called when MediaPlayback is completed */
    public void onCompletion(MediaPlayer player) {

        Log.d(TAG, "MainActivity::onCompletion");

        streamhub.onMediaComplete();
        playheadUpdateScheduler.shutdownNow();
    }

    public void run() {

        try {

            Float seconds = (mediaPlayer.getCurrentPosition() + 0.f) / 1000.f;

            Log.d(TAG, "Position " + seconds);
            streamhub.onTick(seconds);
        }
        catch(IllegalStateException ise){
            Log.d( TAG, "IllegalStateException " + ise.getMessage() );
        }
    }




}
